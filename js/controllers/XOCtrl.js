define(['angular'], function(angular){
    angular.module( 'XOApp.XOCtrl', [])
    .controller('XOCtrl', function XOCtrl ($scope) {
    	var supports_html5_storage = function() {
  			try {
    			return 'localStorage' in window && window['localStorage'] !== null;
  			} catch (e) {
    			return false;
  			}
		}

		$scope.supports_html5_storage = supports_html5_storage();

        var initXO = function() {
       		$scope.parties = (window.localStorage.parties !== undefined) ? JSON.parse(window.localStorage.parties) : [];
            $scope.cols = [];
            for (var i = 0; i < 3; i++) {
                $scope.cols.push(['.', '.', '.']);
            }
            $scope.currentParty = $scope.parties.length;
            $scope.whoWin = null;
            $scope.turn = 'x';
        }

        initXO();

        $scope.clickCell = function(col, row) {
            if ($scope.cols[col][row] != '.' || $scope.whoWin !== null) {
                return;
            }
            $scope.cols[col][row] = $scope.turn;

            $scope.turn = ($scope.turn == 'x') ? 'o' : 'x';

            $scope.canPlay = checkNotFinish();
        }

        var checkNotFinish = function() {
            if ($scope.whoWin === null) {
                for (var i = 0; i < 3; i++) {
                    if (($scope.cols[i][0] == $scope.cols[i][1] &&
                        $scope.cols[i][0] == $scope.cols[i][2] &&
                        $scope.cols[i][0] == 'x') ||
                        ($scope.cols[0][i] == $scope.cols[1][i] &&
                        $scope.cols[0][i] == $scope.cols[2][i] &&
                        $scope.cols[0][i] == 'x')) {
                        $scope.whoWin = 'x';
                        return;
                    }
                    if (($scope.cols[i][0] == $scope.cols[i][1] &&
                        $scope.cols[i][0] == $scope.cols[i][2] &&
                        $scope.cols[i][0] == 'o') ||
                        ($scope.cols[0][i] == $scope.cols[1][i] &&
                        $scope.cols[0][i] == $scope.cols[2][i] &&
                        $scope.cols[0][i] == 'o')) {
                        $scope.whoWin = 'o';
                        return;
                    }
                }
                if (($scope.cols[0][0] == $scope.cols[1][1] &&
                    $scope.cols[0][0] == $scope.cols[2][2] &&
                    $scope.cols[0][0] == 'x') ||
                    ($scope.cols[0][2] == $scope.cols[1][1] &&
                     $scope.cols[0][2] == $scope.cols[2][0] &&
                     $scope.cols[0][2] == 'x')) {
                    $scope.whoWin = 'x';
                    return;
                }
                if (($scope.cols[0][0] == $scope.cols[1][1] &&
                     $scope.cols[0][0] == $scope.cols[2][2] &&
                     $scope.cols[0][0] == 'o') ||
                    ($scope.cols[0][2] == $scope.cols[1][1] &&
                     $scope.cols[0][2] == $scope.cols[2][0] &&
                     $scope.cols[0][2] == 'o')) {
                    $scope.whoWin = 'o';
                    return;
                }
            }
        }

        $scope.saveParty = function() {
            if ($scope.currentParty <= $scope.parties.length) {
                $scope.parties[$scope.currentParty] = $scope.cols;    
            }
            else {
                $scope.parties.push($scope.cols);
            }
            
            window.localStorage.parties = JSON.stringify($scope.parties);

            initXO();
        }

        $scope.changeParty = function(index) {
            $scope.cols = angular.copy($scope.parties[index]);
            $scope.currentParty = index;
            var x = 0,
                o = 0;
            for (var i = 0; i < 3; i++) {
                for (var j = 0; j < 3; j++) {
                    if ($scope.cols[i][j] == 'x') {
                        x++;
                    }
                    if ($scope.cols[i][j] == 'o') {
                        o++;
                    }
                }
            }
            $scope.turn = (x == o) ? 'x' : 'o';
            $scope.canPlay = checkNotFinish();
        }
    });
});