define([
  	'angular',
  	'XOCtrl'
], function (ng) {
  	'use strict';

  	return ng.module('XOApp', [
    	'XOApp.XOCtrl'
  	]);
});